FROM openjdk:17
COPY target/AnyCommand.jar /root/app/AnyCommand.jar
WORKDIR /root/app
CMD ["java", "-jar", "AnyCommand.jar"]
