AnyCommand is a command handler application.

AnyCommand supports parameters, keys with values amd flags. Quoted expressions are recognized as one parameter or value.
There could be multiple values for one key

Example:
    command-name parameter1 "parameter with spaces" -a value1 -b -c -b "key with spaces"  -a "value2 for same key" parameter2
Explanation:
    This test command have an -a as a key, -c as a flag, and a -b as a key and a flag at the same time - it depends on context.
    If after token defined as a key and flag at the same time, goes something that is not other key or flag - the token is recognized as a key with a subsequent value.
    In other case, if after that token there is another key, or flag, or nothing - token recognizes as a flag.
    So, AnyCommand parses given command next way:
        Name: command-name
        Keys: -a [value2 for same key, value1], -b [key with spaces],
        Flags: -b, -c,
        Params: parameter1, parameter with spaces, parameter2,


To add new command create new class implementing "CommandHandler" interface,
 and annotate it as @Component.
There also is interface "BaseCommandHandler", which you could extend with
your own interface, and implement last one, to create subcommands of your command.
Create separate package into "main" package, and put your new classes and interfaces there.

handleCommand(Command command) method give you a simple command, where first word was recognized as a command name, and
all next words were recognized as parameters.
You can define which keys or flags your command will have, and parse command using there methods:
    - Command class have method getCommandKeys(Set<String> keys, Set<String> flags) which finds all keys and flags in given command,
        and returns new Command instance.
    - Then you can call getQuotedCommand() which will recompile command regarding on quotes, so amount of parameters could be decreased,
        as they could be joined together by quotation



