package main.core;

import main.core.exceptions.InvalidCommandException;
import main.core.exceptions.NoSuchCommandException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This interface should be implemented directly by classes 
 * implying to be subcommands, or options of existing commands.
 */
public interface BaseCommandHandler {
    String getName();

    void handleCommand(Command command);

    String getHelp();

    /**
     * @param command Command which have been submitted by user.
     * @param positionOfOption Position of option at command.
     *                         0 is for command name,
     *                         1 is for first option, and so on.
     * @param optionHandlerMap Map which contains handlers specific for
     *                         available options at these point of
     *                         processing chain of options
     */
    default void findAndHandleSubcommand(Command command, int positionOfOption, Map<String, ? extends BaseCommandHandler> optionHandlerMap) {
        command.commandDepth = positionOfOption;
        String commandName;
        if (positionOfOption == 0)
            commandName = command.getName();
        else {
            if (command.getParams().size() < positionOfOption)
                throw new InvalidCommandException("command should have more parameters");
            commandName = command.get(positionOfOption - 1);
        }
        BaseCommandHandler handler = optionHandlerMap.get(commandName);
        if (handler == null) {
            throw new NoSuchCommandException("\"" + commandName + "\" not an option");
        }
        handler.handleCommand(command);
    }

    public static Map<String, BaseCommandHandler> collectCommandListToMap(List<? extends BaseCommandHandler> list) {
        return list.stream().collect(Collectors.toMap(x-> x.getName(),x->x));
    }

}
