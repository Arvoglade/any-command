package main.core;

import main.core.exceptions.CommandException;

import java.util.*;

public class Command {

    private String line;
    private String name;
    private List<String> params;
    private Map<String, List<String>> keys;
    private Set<String> flags;
    private boolean dashPrefixAlwaysKeyOrFlag = true;

    private Set<String> flagsSetting = new HashSet<>();
    private Set<String> keysSetting = new HashSet<>();

    public int commandDepth = 0;

    public String getName() {
        return name;
    }

    public String getLine() {
        return line;
    }

    public String get(int index) {
        return params.get(index);
    }

    public List<String> getParams() {
        return params;
    }

    public Map<String, String> getKeys() {
        Map<String, String> result = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : keys.entrySet()) {
            result.put(entry.getKey(), entry.getValue().get(0));
        }
        return result;
    }

    public Map<String, List<String>> getKeysMultiple() {
        return keys;
    }

    public String getValue(String key) {
        return keys.get(key).get(0);
    }

    public List<String> getValues(String key) {
        return keys.get(key);
    }

    public Set<String> getFlags() {
        return flags;
    }

    public Command(String line) throws CommandException {
        this(line, new HashSet<>(), new HashSet<>(), false);
    }

    public Command(String line, boolean dashPrefixAlwaysKeyOrFlag) throws CommandException {
        this(line, new HashSet<>(), new HashSet<>(), dashPrefixAlwaysKeyOrFlag);
    }

    public Command(String line, Set<String> valueKeys) throws CommandException {
        this(line, valueKeys, new HashSet<>(), true);
    }

    public Command(String line, Set<String> valueKeys, Set<String> flagKeys) throws CommandException {
        this(line, valueKeys, flagKeys, true);
    }

    public Command(String line, Set<String> valueKeys, Set<String> flagKeys, boolean dashPrefixAlwaysKeyOrFlag) throws CommandException {
        this.flagsSetting = flagKeys;
        this.keysSetting = valueKeys;

        this.line = line.replaceAll("\\s+", " ");
        this.dashPrefixAlwaysKeyOrFlag = dashPrefixAlwaysKeyOrFlag;
        this.keys = new HashMap<>();
        this.flags = new HashSet<>();
        this.params = new ArrayList<>();

        initKeysAndParams(this.line.split(" "), valueKeys, flagKeys);
    }

    public Command getCommandKeys(Set<String> valueKeys) {
        Command command = new Command(line, valueKeys);
        command.commandDepth = this.commandDepth;
        return command;
    }

    public Command getCommandFlags(Set<String> flagKeys) {
        Command command = new Command(line, new HashSet<>(), flagKeys);
        command.commandDepth = this.commandDepth;
        return command;
    }


    public Command getCommandKeysFlags(Set<String> valueKeys, Set<String> flagKeys) {
        Command command = new Command(line, valueKeys, flagKeys);
        command.commandDepth = this.commandDepth;
        return command;
    }

    public Command getQuotedCommand() {
        String newLine;
        newLine = dequoteBeforeConstruct(this.line);

        Command command = new Command(newLine, keysSetting, flagsSetting, dashPrefixAlwaysKeyOrFlag);
        command = dequoteAfterConstruct(command);
        command.line = this.line;
        command.commandDepth = this.commandDepth;
        return command;
    }

    private String dequoteBeforeConstruct(String line) {
        StringBuilder builder = new StringBuilder();

        boolean insideQuotes = false;
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            if (c == '"' && insideQuotes) {
                insideQuotes = false;
            } else if (c == '"' && !insideQuotes) {
                insideQuotes = true;
            } else if (insideQuotes && c == ' ') {
                builder.append("\"");
            } else {
                builder.append(c);
            }
        }
        return builder.toString();
    }

    private Command dequoteAfterConstruct(Command command) {

        for (int i = 0; i < command.getParams().size(); i++) {
            String param = command.getParams().get(i);
            param = param.replaceAll("\"", " ");
            command.getParams().set(i, param);
        }
        Map<String, List<String>> newKeyMap = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : command.keys.entrySet()) {
            List<String> values = entry.getValue().stream().map(v -> v.replaceAll("\"", " ")).toList();
            newKeyMap.put(entry.getKey(), values);
        }
        command.keys = newKeyMap;

        return command;
    }

    private void initKeysAndParams(String[] words, Set<String> keys, Set<String> flags) throws CommandException {
        name = words[0];

        String temp = null;
        OUTER:
        for (int i = words.length - 1; i > 0; i--) {
            if (temp != null) {
                for (String key : keys) {
                    if (words[i].startsWith(key)) {
                        if (words[i].length() == key.length()) {
                            if (this.keys.containsKey(key)) {
                                this.keys.get(key).add(temp);
                            } else {
                                this.keys.put(key, new ArrayList<>(List.of(temp)));
                            }
                            temp = null;
                            continue OUTER;
                        } else {
                            this.params.add(temp);
                            temp = null;
                            String value = words[i].substring(key.length());
                            if (this.keys.containsKey(key)) {
                                this.keys.get(key).add(value);
                            } else {
                                this.keys.put(key, new ArrayList<>(List.of(value)));
                            }
                            continue OUTER;
                        }
                    }
                }
                for (String flag : flags) {
                    if (words[i].startsWith(flag)) {
                        if (words[i].length() == flag.length()) {
                            this.params.add(temp);
                            temp = null;
                            this.flags.add(flag);
                            continue OUTER;
                        } else {
                            this.params.add(temp);
                            temp = null;
                            this.params.add(words[i].substring(flag.length()));
                            this.flags.add(flag);
                            continue OUTER;
                        }
                    }
                }
            } else {
                for (String key : keys) {
                    if (words[i].startsWith(key)) {
                        if (words[i].length() == key.length()) {
                            if (!flags.contains(key))
                                throw new CommandException("after key \"" + key + "\" should be a value");
                        } else {
                            String value = words[i].substring(key.length());
                            if (this.keys.containsKey(key)) {
                                this.keys.get(key).add(value);
                            } else {
                                this.keys.put(key, new ArrayList<>(List.of(value)));
                            }
                            continue OUTER;
                        }
                    }
                }
                for (String flag : flags) {
                    if (words[i].startsWith(flag)) {
                        if (words[i].length() == flag.length()) {
                            this.flags.add(flag);
                            continue OUTER;
                        } else {
                            this.flags.add(flag);
                            params.add(words[i].substring(flag.length()));
                            continue OUTER;
                        }
                    }
                }

            }

            if (dashPrefixAlwaysKeyOrFlag) {
                if (words[i].startsWith("-")) {
                    throw new CommandException("Invalid key or flag \"" + words[i].substring(0,2) + "\"");
                }
            }

            if (temp != null) {
                params.add(temp);
                temp = null;
            }

            temp = words[i];

        }
        if (temp != null)
            params.add(temp);

        List<String> tempParams = new ArrayList<>();
        for (int i = params.size() - 1; i >= 0; i--) {
            tempParams.add(params.get(i));
        }
        params = tempParams;

    }


}
