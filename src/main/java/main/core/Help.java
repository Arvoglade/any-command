package main.core;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class Help implements HeadCommandHandler {

    private Set<String> flags = new HashSet<>(java.util.List.of("-l"));

    private final ApplicationContext applicationContext;


    public Help(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }


    @Override
    public String getName() {
        return "help";
    }

    @Override
    public void handleCommand(Command command) {

        command = command.getCommandKeysFlags(new HashSet<>(), flags);

        if (command.getFlags().contains("-l")) {
            list();
            return;
        }
        if (command.getParams().size() == 1) {
            Runner runner = applicationContext.getBean(Runner.class);
            HeadCommandHandler commandHandler = runner.commandHandlerMap.get(command.get(0));
            if (commandHandler != null) {
                System.out.println(commandHandler.getHelp());
            } else {
                System.err.println("Command " + command.get(0) + " no found");
            }

            return;
        }
        System.out.println("""
                To show this message type "help"
                To list all commands type "help -l"
                To show help about specific command type "help <commandName>"
                """);
    }

    @Override
    public String getHelp() {
        return "Type \"help\" to get all commands list with description. Type \"help <commandName>\" to get description of this command";
    }

    private void list() {
        System.out.println("=== HELP ===");
        Runner runner = applicationContext.getBean(Runner.class);
        Map<String, HeadCommandHandler> sortedMap = new TreeMap<>(runner.commandHandlerMap);
        for (Map.Entry<String, HeadCommandHandler> entry : sortedMap.entrySet()) {
            System.out.println(">>> " + entry.getKey());
            System.out.println(entry.getValue().getHelp());
            System.out.println();
        }
    }
}
