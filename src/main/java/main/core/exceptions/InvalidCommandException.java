package main.core.exceptions;

public class InvalidCommandException extends CommandException {
    public InvalidCommandException(String message) {
        super(message);
    }
}
