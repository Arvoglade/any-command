package main.core.exceptions;

public class NoSuchCommandException extends CommandException {
    public NoSuchCommandException(String message) {
        super(message);
    }
}
