package main.core;

import main.core.exceptions.CommandException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

@Component
public class Runner implements CommandLineRunner {
    public Map<String, HeadCommandHandler> commandHandlerMap = new HashMap<>();

    public Runner(List<HeadCommandHandler> commandHandlerList) {
        commandHandlerMap.putAll(commandHandlerList.stream().collect(Collectors.toMap(HeadCommandHandler::getName, ch -> ch)));
    }

    public void run(String[] args) throws IOException {
        runConsoleInterface();
    }

    private void runConsoleInterface() {
        System.out.println("Ready to accept your commands");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String line = scanner.nextLine().trim();
            if (line.isBlank() || line.isEmpty())
                continue;
            Command command = new Command(line);
            HeadCommandHandler commandHandler = commandHandlerMap.get(command.getName());
            if (commandHandler == null) {
                System.err.println("\"" + command.getName() + "\"" + " not a command"
                        + ". Type \"help -l\" to list them"
                        );
                continue;
            }
            try {
                commandHandler.handleCommand(command);
            } catch (CommandException e) {
                System.err.println(e.getMessage());
            } catch (Exception e) {
                System.err.println("Command has throw/home/solibr/YandexDisk/IdeaProjects/AnyCommand/src/main/java/main/core/exceptionsn an exception");
                e.printStackTrace();
            }
        }
    }
}
