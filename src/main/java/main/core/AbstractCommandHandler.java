package main.core;

import main.core.exceptions.NoSuchCommandException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractCommandHandler implements BaseCommandHandler, ApplicationContextAware {

    protected Map<String, BaseCommandHandler> subcommandHandlersMap = new HashMap<>();

    public AbstractCommandHandler() {
    }

    @Override
    public void handleCommand(Command command) {

        //handle(command);

        // FIXME - at this point it's not recognizable are next words parameters or just keys
        //  should to find a way to decide what to do: make some action now or find subcommand
        if (command.getParams().size() == 0) {
            handle(command);
        } else if (command.getParams().size() >= 1) {
            try {
                findAndHandleSubcommand(command, ++command.commandDepth, subcommandHandlersMap);
            } catch (NoSuchCommandException e) {
                handleIfSubcommandNotFound(command);
            }
        }
    }

    private void handleIfSubcommandNotFound(Command command) {
        System.out.println("\"" + command.getName() + "\"" + " command doesn't have subcommand \"" + command.getParams().get(command.commandDepth - 1) + "\"");
    }

    // TODO - rename method name later
    public abstract void handle(Command command);

    // TODO - should I leave this method here or not?
    @Override
    public String getHelp() {
        return null;
    }

    public abstract Class<?> getSubcommandsInterface();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Class<?> subcommandInterface = getSubcommandsInterface();
        if (subcommandInterface != null) {
            Map<String, BaseCommandHandler> beansOfType =
                    (Map<String, BaseCommandHandler>) applicationContext.getBeansOfType(subcommandInterface);

            this.subcommandHandlersMap = beansOfType.entrySet().stream()
                    .map(entry -> entry.getValue()).collect(Collectors.toMap(h -> h.getName(), h -> h));
        }
    }
}
