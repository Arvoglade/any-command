package main.core;

import org.springframework.stereotype.Component;

@Component
public class Exit implements HeadCommandHandler {
    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public void handleCommand(Command command) {
        System.exit(0);
    }

    @Override
    public String getHelp() {
        return "Exiting the program";
    }
}
