package main.core;

/**
 * This interface should be only implemented by classes
 * intended to be called as an independence command.
 */
public interface HeadCommandHandler extends BaseCommandHandler {

}
