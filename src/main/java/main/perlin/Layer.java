package main.perlin;

public class Layer {

    public float[][] arr;
    public float max;
    public float min;

    public float diff;

    public Layer(int width, int height) {
        arr = new float[width][height];
    }
}
