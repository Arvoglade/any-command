package main.perlin;


import io.github.overrun.perlinoisej.PerlinNoise;
import main.core.Command;
import main.core.HeadCommandHandler;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Component
public class PerlinCommandHandler implements HeadCommandHandler {

    static int width = 1000;
    static int height = 1000;

    static int z = 0;
    static int x = 0;
    static int y = 0;

    @Override
    public String getName() {
        return "perlin";
    }

    @Override
    public void handleCommand(Command command) {



        int scale1 = Integer.parseInt(command.getParams().get(0));
        float w1 = Float.parseFloat(command.getParams().get(1));
        int scale2 = Integer.parseInt(command.getParams().get(2));
        float w2 = Float.parseFloat(command.getParams().get(3));

        int seed = (int) (Math.random() * 255);

        Layer l1 = createLayer(seed, scale1);
        Layer l2 = createLayer(seed, scale2);

        BufferedImage image = createImage(l1, w1, l2, w2);
        writeToFile(image);

    }

    public Layer createLayer(int seed, int scale) {
        Layer l = new Layer(width, height);

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                float perlinValue = PerlinNoise.noise3seed(((float)i) / scale, ((float)j) / scale, 0, x, y, z, seed);

                if (perlinValue > l.max)
                    l.max = perlinValue;
                if (perlinValue < l.min)
                    l.min = perlinValue;

                l.arr[i][j] = perlinValue;
            }
        }
        l.diff = l.max - l.min;
        return l;
    }

    public BufferedImage createImage(Layer layer) {

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int b = (int) ((layer.arr[i][j] - layer.min) / layer.diff * 255);
                int rgb = getRGB(b, b, b);
                image.setRGB(i, j, rgb);
            }
        }
        return image;
    }

    public BufferedImage createImage(Layer layer1, float w1, Layer layer2, float w2) {

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        float resMax = layer1.max + layer2.max;
        float resMin = layer1.min * w1 + layer2.min * w2;
        float resDiff = resMax - resMin;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                float resVal = layer1.arr[i][j] * w1 + layer2.arr[i][j] * w2;
                int b = (int) ((resVal - resMin) / resDiff * 255);


                if (b > 150)
                    b = 200;
                else
                    b = 50;

                int rgb = getRGB(b, b, b);
                image.setRGB(i, j, rgb);
            }
        }
        return image;
    }

    public void writeToFile(BufferedImage image) {
        //Files.write(Paths.get("C:/Users/Solibr/Desktop"), , )
        File file = new File("C:/Users/Solibr/Desktop/img.jpg");
        try {
            ImageIO.write(image, "jpg", file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    private int getRGB(int red, int green, int blue) {
        int res = 0;
        res = res | red;
        res = res << 8;
        res = res | green;
        res = res << 8;
        res = res | blue;
        return res;
    }

    private static String getMask(int number) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 32; i++) {
            int bit = number & 1;
            if (bit == 1) {
                sb.append("1");
            } else sb.append("0");
            number = number >> 1;
        }
        sb.reverse();

        return sb.toString();
    }

    @Override
    public String getHelp() {
        return "perlin <seed (integer)>";
    }
}
