package main.sort.algorithm;

import main.sort.SortingAlgorithm;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class HeapSort implements SortingAlgorithm {
    @Override
    public String getName() {
        return "heap";
    }

    @Override
    public List<Integer> sort(List<Integer> list) {

        int heapSize = list.size();

        heapify(list, 0, heapSize);

        while (heapSize > 1) {
            int temp = list.get(heapSize - 1);
            list.set(heapSize - 1, list.get(0));
            list.set(0, temp);
            heapSize--;
            siftDown(list, 0, heapSize);
        }

        return list;
    }

    private void heapify(List<Integer> list, int i, int heapSize) {
        int left = i * 2 + 1;
        int right = i * 2 + 2;
        boolean hasLeftChild = left < heapSize;
        boolean hasRightChild = right < heapSize;
        if (hasLeftChild) {
            heapify(list, left, heapSize);
            if (list.get(left) > list.get(i)) {
                int temp = list.get(i);
                list.set(i, list.get(left));
                list.set(left, temp);
            }
        }
        if (hasRightChild) {
            heapify(list, right, heapSize);
            if (list.get(right) > list.get(i)) {
                int temp = list.get(i);
                list.set(i, list.get(right));
                list.set(right, temp);
            }
        }
    }

    private void siftDown(List<Integer> list, int i, int heapSize) {
        int left = i * 2 + 1;
        int right = i * 2 + 2;
        boolean hasLeftChild = left < heapSize;
        boolean hasRightChild = right < heapSize;
        if (hasLeftChild) {
            if ((list.get(i) < list.get(left)) && ((!hasRightChild) || (list.get(left) > list.get(right)))) {
                int temp = list.get(i);
                list.set(i, list.get(left));
                list.set(left, temp);
                siftDown(list, left, heapSize);
            } else if (hasRightChild && list.get(i) < list.get(right)) {
                int temp = list.get(i);
                list.set(i, list.get(right));
                list.set(right, temp);
                siftDown(list, right, heapSize);
            }
        }
    }
}
