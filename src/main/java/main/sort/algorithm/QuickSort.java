package main.sort.algorithm;

import main.core.Command;
import main.core.exceptions.CommandException;
import main.sort.ShuffledListSupplier;
import main.sort.SortingAlgorithm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class QuickSort implements SortingAlgorithm {

    private Random random = new Random();
    @Override
    public String getName() {
        return "quick";
    }


    public List<Integer> sort(List<Integer> inputList) {
        innerSort(inputList, 0, inputList.size());
        return inputList;
    }


    private void innerSort(List<Integer> list, int beginIdx, int endIdx) {
        if (beginIdx < endIdx - 1) {
            int pivotIdx = random.nextInt(beginIdx, endIdx);
            int pivotVal = list.get(pivotIdx);
            List<Integer> leftList = new ArrayList<>();
            List<Integer> rightList = new ArrayList<>();
            for (int i = beginIdx; i < endIdx; i++) {
                if (i == pivotIdx) {
                    continue;
                }
                if (list.get(i) < pivotVal) {
                    leftList.add(list.get(i));
                } else {
                    rightList.add(list.get(i));
                }
            }
            int j = beginIdx;
            for (int i = 0; i < leftList.size(); i++) {
                list.set(j++ , leftList.get(i));
            }
            list.set(j++, pivotVal);
            for (int i = 0; i < rightList.size(); i++) {
                list.set(j++, rightList.get(i));
            }

            innerSort(list, beginIdx, beginIdx + leftList.size());
            innerSort(list, beginIdx + leftList.size() + 1, endIdx);
        }

    }
}
