package main.sort.algorithm;

import main.sort.SortingAlgorithm;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SelectionSort implements SortingAlgorithm {
    @Override
    public String getName() {
        return "selection";
    }

    @Override
    public List<Integer> sort(List<Integer> list) {

        for (int start = 0; start < list.size() - 1; start++) {

            int min = list.get(start);
            int minIdx = start;
            for (int i = start + 1; i < list.size(); i++) {
                if (list.get(i) < min) {
                    min = list.get(i);
                    minIdx = i;
                }
            }
            if (minIdx != start) {
                list.set(minIdx, list.get(start));
                list.set(start, min);
            }

        }

        return list;
    }
}
