package main.sort.algorithm;

import main.core.Command;
import main.core.exceptions.CommandException;
import main.sort.ShuffledListSupplier;
import main.sort.SortingAlgorithm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BubbleSort implements SortingAlgorithm {
    @Override
    public String getName() {
        return "bubble";
    }

    public List<Integer> sort(List<Integer> list) {

        List<Integer> sortingList = new ArrayList<>(list);

        for (int i = 0; i < list.size() - 1; i++) {

            for (int j = 0; j < list.size() - 1 - i; j++) {
                int a = sortingList.get(j);
                int b = sortingList.get(j + 1);
                if (a > b) {
                    sortingList.set(j, b);
                    sortingList.set(j + 1, a);
                }
            }
        }
        return sortingList;

    }


}
