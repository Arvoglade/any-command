package main.sort;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class ShuffledListSupplier {

    public List<Integer> getShuffledList(int count) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        return list;
    }

}
