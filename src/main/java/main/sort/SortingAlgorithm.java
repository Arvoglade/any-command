package main.sort;

import main.core.Command;

import java.util.List;

public interface SortingAlgorithm {

    String getName();

    List<Integer> sort(List<Integer> list);

}
