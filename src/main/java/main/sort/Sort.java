package main.sort;

import main.core.Command;
import main.core.HeadCommandHandler;
import main.core.exceptions.InvalidCommandException;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class Sort implements HeadCommandHandler {

    private final Map<String, SortingAlgorithm> sortAlgorithms;
    private final Set<String> flags = new HashSet<>(Set.of("-l", "-v", "-h"));
    private final Set<String> keys = new HashSet<>(Set.of("-a", "-s"));

    private final ShuffledListSupplier shuffledListSupplier;

    public Sort(List<SortingAlgorithm> sortCommandHandlerList, ShuffledListSupplier shuffledListSupplier) {
        this.sortAlgorithms = sortCommandHandlerList.stream()
                .collect(Collectors.toMap( h -> h.getName(), h -> h));
        this.shuffledListSupplier = shuffledListSupplier;
    }

    @Override
    public String getName() {
        return "sort";
    }

    @Override
    public void handleCommand(Command command) {
        command = command.getCommandKeysFlags(keys, flags);


        if (command.getFlags().contains("-h")) {
            System.out.println(getFullHelp());
            return;
        }

        if (command.getFlags().contains("-l")) {
            System.out.println("List of sorting algorithms:");
            sortAlgorithms.keySet().forEach(x -> System.out.println("\t" + x));
            return;
        }

        SortingAlgorithm algorithm;
        if (command.getKeys().containsKey("-a")) {
            algorithm = sortAlgorithms.get(command.getKeys().get("-a"));
            if (algorithm == null) {
                throw new InvalidCommandException("No such algorithm");
            }
        } else {
            algorithm = sortAlgorithms.values().stream()
                    .findAny()
                    .orElseThrow(() -> new InvalidCommandException("No algorithms"));
            System.out.println("using " + algorithm.getName() + " sort algorithm");
        }

        int size = 10;
        if (command.getKeys().containsKey("-s")) {
            try {
                size = Integer.parseInt(command.getKeys().get("-s"));
            } catch (NumberFormatException e) {
                throw new InvalidCommandException("Invalid size of array");
            }
        }

        boolean showListBeforeAndAfter = command.getFlags().contains("-v");

        List<Integer> list = shuffledListSupplier.getShuffledList(size);
        if (showListBeforeAndAfter) {
            System.out.println("List before sort: " + list);
        }

        long startTime = System.currentTimeMillis();
        List<Integer> sortedList = algorithm.sort(list);
        System.out.println("TIME : " + (System.currentTimeMillis() - startTime));

        if (showListBeforeAndAfter) {
            System.out.println("List after sort: " + sortedList);
        }


    }

    @Override
    public String getHelp() {
        return """
                test different sorting algorithms \"sort -h\" for more info
                """;
    }

    private String getFullHelp() {
        return """
                Syntax:
                sort -a <name of sorting algorithm> [-s <size of list to sort>] [-v]
                -a set algorithm
                -s set size of array to sort. default 10
                -v To see list before and after sorting
                -h To get this message
                -l To see available algorithms names
                """;
    }
}
