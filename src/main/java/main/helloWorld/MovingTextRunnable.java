package main.helloWorld;

public class MovingTextRunnable implements Runnable {

    private int line;

    public MovingTextRunnable(int line) {
        this.line = line;
    }


    @Override
    public void run() {
        try {
            HelloWorld.printMovingText(line);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
