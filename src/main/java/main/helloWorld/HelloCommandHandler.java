package main.helloWorld;

import main.core.Command;
import main.core.HeadCommandHandler;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

@Component
public class HelloCommandHandler implements HeadCommandHandler {
    private Set<String> keys = new HashSet<>(List.of("-s"));

    @Override
    public String getName() {
        return "hello";
    }

    @Override
    public void handleCommand(Command command) {
        command = command.getCommandKeys(keys);

        HelloWorld.initChars(true);
        int fontSize = 30;
        if (command.getKeys().containsKey("-s")) {
            fontSize = Integer.parseInt(command.getKeys().get("-s"));
        }

        String line = String.join(" ", command.getParams());
        if (line.length() == 0) {
            line = "   Hello world!";
        }

        Thread thread = new Thread(new BigHelloWorld(line, fontSize));
        thread.start();
        new Scanner(System.in).nextLine();
        thread.interrupt();

    }

    @Override
    public String getHelp() {
        return null;
    }
}
