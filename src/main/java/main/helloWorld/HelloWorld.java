package main.helloWorld;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HelloWorld {

    static Random random = new Random();
    static List<Character> chars = new ArrayList<>();
    static List<Thread> threads = new ArrayList<>();
    static int threadsCount = 20;

    public static void main(String[] args) throws InterruptedException {

        initChars(true);
        //chars.forEach(System.out::println);

        //simplePrint();
        //printSomething();
        //printMovingText(20);
        //print20MovingTexts();

    }

    static void print20MovingTexts() {
        for (int i = 0; i < threadsCount; i++) {
            Runnable runnable = new MovingTextRunnable(i);
            threads.add(new Thread(runnable));
        }
        threads.forEach(Thread::start);
    }

    static void printMovingText(int line) throws InterruptedException {

        System.out.print("\n".repeat(50));

        //line = 20;
        int direction = -1;

        int maxColumn = 100;
        int minColumn = 5;

        int column = random.nextInt(maxColumn - minColumn) + minColumn;


        while (true) {

            synchronized (System.out) {
                printStringAt("            ", line, column);
                if (direction < 0 && column < minColumn) {
                    direction = 1;
                } else if (column > maxColumn) {
                    direction = -1;
                }

                column += direction;
                printStringAt("Hello World!", line, column);
            }

            Thread.sleep(50);
        }
    }

    static void printSomething() {
          //      \033[<L>;<C>H
        String str = getRandomString(10);
        System.out.print("\n".repeat(15));
        printStringAt(str,10, 10);
        printStringAt("",20, 20);
    }

    static void printStringAt(String string, int line, int column) {
        String caretMoving = String.format("\033[%d;%dH", line, column);
        System.out.print(caretMoving);
        System.out.print(string);
    }


    static void simplePrint() throws InterruptedException {
        while (true) {
            Thread.sleep(50);
            System.out.println(getRandomString(150));
        }
    }

    static String getRandomString(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++)
            sb.append(getRandomChar());
        return sb.toString();
    }

    static char getRandomChar() {
        return chars.get(random.nextInt(chars.size()));
    }

    static void initChars(boolean onlyVisibleChars) {
        int i = 0;
        while (i < 256) {
            char c = (char) i;
            if (!onlyVisibleChars || ((i >= 33 && i <= 126) || (i >= 161 && i <= 255))) {
                chars.add((char)i);
            }
            i++;
        }
    }
}

/*
! ~
¡ ÿ
 */