package main.helloWorld;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BigHelloWorld implements Runnable {


    //private BufferedImage image;
    //private Graphics2D graphics;

    private List<List<Boolean>> grid = new ArrayList<>();
    private int brightnessLimit = 50;
    private int printerWidth = 150;

    //private static volatile boolean isInterrupted = false;

    private String line;
    private int fontSize;

    public BigHelloWorld(String line, int fontSize) {
        this.line = line;
        this.fontSize = fontSize;
    }

    public void run() {
        runString(line, fontSize);
    }

    private void runString(String line, int fontSize) {
        System.out.print("\n".repeat(50));
        initGrid(line, fontSize);
        int position = 0;

        while (true) {

            for (int row = 0; row < grid.size(); row++) {
                String str = fillGridRowByRandomChar(row, position);
                HelloWorld.printStringAt(str, row, 1);
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                return;
            }

            for (int i = 0; i < grid.size(); i++) {
                String str = " ".repeat(printerWidth);
                HelloWorld.printStringAt(str, i, 1);
            }

            position++;
            if (position >= grid.get(0).size()) {
                position = 0;
            }

        }

    }

    private String fillGridRowByRandomChar(int row, int startPosition) {
        List<Boolean> thisRow = grid.get(row);
        StringBuilder sb = new StringBuilder();

        for (int column = startPosition; column < startPosition + printerWidth; column++) {

            int columnToRead;
            // ==
            columnToRead = column;
            while (!(columnToRead < thisRow.size())) {
                columnToRead -= thisRow.size();
            }

            if (thisRow.get(columnToRead))
                sb.append(HelloWorld.getRandomChar());
            else
                sb.append(" ");
        }
        return sb.toString();
    }

    private void initGrid(String line, int fontSize) {
        BufferedImage image = createImage(line, fontSize);

        try {
            ImageIO.write(image, "jpg", new File("img.jpg"));
        } catch (IOException e) {throw new RuntimeException(e);}

        image.getHeight();
        for (int row = 0; row < image.getHeight(); row++) {
            grid.add(new ArrayList<>());
            List<Boolean> thisRowList = grid.get(row);
            for (int column = 0; column < image.getWidth(); column++) {

                int rgb = image.getRGB(column, row);
                int brightness = getBrightness(rgb);

                if  (brightness > brightnessLimit)
                    thisRowList.add(true);
                else
                    thisRowList.add(false);
            }
        }
    }

    private int getBrightness(int rgb) {
        int red = (rgb >> 16) & 0xff;
        int green = (rgb >> 8) & 0xff;
        int blue = rgb & 0xff;
        return red + green + blue;
    }

    private BufferedImage createImage(String line, int fontSize) {
        if (fontSize == 0)
            fontSize = 12;
        int width = (int) (fontSize * (line.length() + 1) / 1.5);
        int height = (int) (fontSize * 1.3);
        int lineBeginX = 2;
        int lineBeginY = 0;

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = image.createGraphics();

        Font myFont = new Font("Georgia", Font.ITALIC, fontSize);
        graphics.setFont(myFont);
        graphics.setColor(Color.WHITE);
        graphics.drawString(line, lineBeginX, lineBeginY + fontSize);

        return image;
    }

}
