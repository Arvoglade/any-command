package main.fileUtil.group;

import main.core.AbstractCommandHandler;
import main.core.Command;
import main.fileUtil.FilesCommandHandler;
import main.fileUtil.FilesSubcommandHandler;
import org.springframework.stereotype.Component;

@Component
public class GroupCommandHandler extends AbstractCommandHandler implements FilesSubcommandHandler {
    @Override
    public void handle(Command command) {

    }
    @Override
    public Class<?> getSubcommandsInterface() {
        return GroupSubcommandHandler.class;
    }

    @Override
    public String getName() {
        return "group";
    }
}
