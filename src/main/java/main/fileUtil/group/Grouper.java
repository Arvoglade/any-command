package main.fileUtil.group;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class Grouper {

    private Set<String> sourcesPath;
    private String destinationPath;

    private final Function<String, String> groupingStrategy;

    public Grouper(Set<String> sourcesPaths, String destinationPath, Function<String, String> groupingStrategy) {
        this.destinationPath = destinationPath;
        this.sourcesPath = sourcesPaths;
        this.groupingStrategy = groupingStrategy;
    }

    public void group() throws InterruptedException {
        for (String sourcePath : sourcesPath) {
            try {
                groupRecursive(Paths.get(sourcePath));
            } catch (IOException e) {
                System.err.println("Files reading error: " + sourcesPath);
                e.printStackTrace();
            }
        }
    }

    private void groupRecursive(Path sourcepath) throws IOException, InterruptedException {

        List<Path> paths = Files.list(sourcepath).toList();
        for (Path path : paths) {
            if (Files.isDirectory(path)) {
                try {
                    groupRecursive(path);
                } catch (IOException e) {
                    System.err.println("Files reading error: " + path);
                    e.printStackTrace();
                }
            } else {
                if (Thread.currentThread().isInterrupted()) {
                    throw new InterruptedException();
                }

                String newFilePath = groupingStrategy.apply(path.toString());

                try {
                    moveFile(path, Paths.get(newFilePath));
                } catch (IOException e) {
                    System.err.println("Files moving error: " + path);
                }
            }
        }

    }

    private void moveFile(Path source, Path destination) throws IOException {
        Path parentPath = destination.getParent();
        if (!Files.exists(parentPath)) {
            Files.createDirectory(parentPath);
        }  else if (Files.exists(destination)) {
            Path nextDestination;
            int i = 1;
            do {
                nextDestination = getNextDestination(destination, i++);
            } while (Files.exists(nextDestination));
            destination = nextDestination;
        }
        Files.move(source, destination);
    }

    private Path getNextDestination(Path destination, int i) {
        String destinationString = destination.toString();
        String suffixExcluded = destinationString.substring(0, destinationString.lastIndexOf('.'));
        String suffix = destinationString.substring(destinationString.lastIndexOf('.') + 1);
        String newPath = suffixExcluded + " " + i + "." + suffix;
        return Paths.get(newPath);
    }
}
