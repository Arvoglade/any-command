package main.fileUtil.group.abc;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class AbcGroupingWorker {

    private final Path source;

    public AbcGroupingWorker(Path source) {
        this.source = source;
    }

    public void group() {

        List<Path> folders = new ArrayList<>();
        try (Stream<Path> p = Files.list(source)) {
            folders = p.toList();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        folders.stream()
                .forEach(folder -> {
                    String firstLetter = String.valueOf(folder.getFileName().toString().charAt(0));
                    Path newSeparateFolder = source.resolve(firstLetter);
                    if (!Files.exists(newSeparateFolder)) {
                        try {
                            Files.createDirectory(newSeparateFolder);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    try {
                        Files.move(folder, newSeparateFolder.resolve(folder.getFileName()));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });


    }



}
