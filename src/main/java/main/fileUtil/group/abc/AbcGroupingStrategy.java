package main.fileUtil.group.abc;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;

public class AbcGroupingStrategy implements Function<String, String> {
    private final String destinationPath;
    public AbcGroupingStrategy(String destinationPath) {
        this.destinationPath = destinationPath;
    }

    @Override
    public String apply(String s) {
        Path path = Paths.get(s);
        String newFilePath;

        String separateFolderName;
        char firstLetter = path.getFileName().toString().charAt(0);
        if (isValid(firstLetter)) {
            separateFolderName = String.valueOf(firstLetter);
            separateFolderName = separateFolderName.toLowerCase();
        } else {
            separateFolderName = "!other";
        }
        newFilePath = destinationPath + File.separator
                + separateFolderName + File.separator
                + path.getFileName().toString();

        return newFilePath;
    }

    private boolean isValid(char firstLetter) {
        return (firstLetter >= 'a' && firstLetter <= 'z') || (firstLetter >= 'A' && firstLetter <= 'Z');
    }
}
