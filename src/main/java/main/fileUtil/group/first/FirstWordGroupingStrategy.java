package main.fileUtil.group.first;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;

public class FirstWordGroupingStrategy implements Function<String, String> {

    private final String destinationPath;
    private final boolean isSmart;


    public FirstWordGroupingStrategy(String destinationPath, boolean isSmart) {
        this.destinationPath = destinationPath;
        this.isSmart = isSmart;
    }


    @Override
    public String apply(String s) {

        Path path = Paths.get(s);
        String newFilePath;
        if (!isSmart || path.getFileName().toString().indexOf(' ') > 0) {
            String separateFolderName = getFirstWord(path);
            newFilePath = destinationPath + File.separator
                    + separateFolderName + File.separator
                    + path.getFileName().toString();
        }
        else {
            newFilePath = destinationPath + File.separator
                    + "! suspicious - have no valuable name" + File.separator
                    + path.getFileName().toString();
        }

        return newFilePath;
    }

    private String getFirstWord(Path path) {
        return path.getFileName().toString().split(" ")[0];
    }
}
