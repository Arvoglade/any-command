package main.fileUtil.group.first;

import main.core.Command;
import main.fileUtil.group.GroupSubcommandHandler;
import main.fileUtil.group.Grouper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;


@Component
public class FirstCommandHandler implements GroupSubcommandHandler {

    private final Set<String> keys = new HashSet<>(List.of("-d", "-s", "-o"));
    private final Set<String> flags = new HashSet<>();
    private final Set<String> helpWords = new HashSet<>(List.of("help", "-h", "h", "--help"));
    private final Set<String> optionsSet = new HashSet<>(List.of("standard", "smart"));

    private boolean isSmart = true;

    @Override
    public String getName() {
        return "first";
    }

    @Override
    public void handleCommand(Command command) {

        command = command.getCommandKeysFlags(keys, flags).getQuotedCommand();

        List<String> sourcePaths = command.getKeysMultiple().get("-s");
        String destinationPath = command.getKeys().get("-d");
        String option = command.getKeys().get("-o");

        if (command.getParams().size() > 1 && helpWords.contains(command.getParams().get(1))) {
            System.out.println(getManual());
            return;
        }
        if (sourcePaths == null || sourcePaths.size() == 0) {
            System.err.println("missing source paths. Type \"files group help\" to get help");
            return;
        }
        if (destinationPath == null) {
            System.err.println("missing destination path. Type \"files group help\" to get help");
            return;
        }

        if (option == null || option.equals("smart")) {
            isSmart = true;
        } else if (option.equals("standard")) {
            isSmart = false;
        } else {
            System.err.println("incorrect option (-o). Only \"smart\" or \"standard\" allowed");
        }

        System.out.println("sources:");
        sourcePaths.forEach(source -> System.out.println("\t" + source));
        System.out.println("dest:");
        System.out.println("\t" + destinationPath);

        // Add running in another thread
        Grouper grouper = new Grouper(new HashSet<>(sourcePaths), destinationPath, new FirstWordGroupingStrategy(destinationPath, isSmart));

        Thread thread = new Thread(() -> {
            try {
                grouper.group();
                System.out.println("Grouping completed. Press enter");
            } catch (InterruptedException e) {
                System.out.println("Grouping interrupted");
            }
        });
        thread.start();
        System.out.println("Grouping... Press enter to stop");
        new Scanner(System.in).nextLine();
        thread.interrupt();

    }

    @Override
    public String getHelp() {
        return """
        group files by first word in filename and move files into separate folders""";
    }

    private String getManual() {
        return """
        group files by first word in filename and move files into separate folders
        This could be done for multiple source paths, and all separate folders will be moved to destination folder
        Syntax:
            group -s <source path1> -s <source path2> -s <source path3> ... -d <destination folder> [-o <standard/smart>]
            -o option: "standard" - default, smart - move files with no spaces to another separate folder "! suspicious - have no valuable name"
        """;
    }
}
