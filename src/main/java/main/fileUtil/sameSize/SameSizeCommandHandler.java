package main.fileUtil.filesSubcommandHandlers;

import main.core.Command;
import main.fileUtil.FilesSubcommandHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class SameSizeCommandHandler implements FilesSubcommandHandler {

    HashMap<Long, Path> mapFileSizes = new HashMap<>();
    private boolean recursively = false;
    private long minSize = 0;
    private long maxSize = Long.MAX_VALUE;

    private Set<String> keys = new HashSet(List.of("-max", "-min"));
    private Set<String> flags = new HashSet(List.of("-r", "--help"));


    @Override
    public String getName() {
        return "same-size";
    }

    @Override
    public void handleCommand(Command command){

        command = new Command(command.getLine(), keys, flags);
        if (command.getFlags().contains("--help")) {
            System.out.println(help());
            return;
        }

        Path path;
        List<String> words = command.getParams();
        StringBuilder pathSb = new StringBuilder();
        words.stream().forEach(x -> {
            pathSb.append(" ");
            pathSb.append(x);
        });
        path = Paths.get(pathSb.toString().trim());

        recursively = command.getFlags().contains("-r");
        if (command.getKeys().containsKey("-max")) {
            maxSize = Long.parseLong(command.getKeys().get("-max"));
        }
        if (command.getKeys().containsKey("-min")) {
            minSize = Long.parseLong(command.getKeys().get("-min"));
        }

        findSame(path);
        mapFileSizes.clear();

    }

    @Override
    public String getHelp() {
        return """
    Accept path where to find files with same size, returns list of fileNames with same size. Type "SameSize --help" for more info""";
    }

    private String help() {
        return """
    Usage:
    \tSameSize [-r] [--min minimumFileSizeToFind] [--max maximumFileSizeToFind] [path]
    \t-r - recursively
    \tpath - if not specified, it will search in same folder where application is located
    Example:
    \tSameSize -r C:/someFolder/some folder with spaces
    Accept path where to find files with same size
    Returns list of fileNames with same size""";
    }

    public void findSame(Path path) {
        try {
            Files.list(path).forEach(p -> {
                try {
                    if (!Files.isDirectory(p)) {
                        long size = Files.size(p);
                        if (mapFileSizes.containsKey(size)) {
                            System.out.println(size + "\t" + getFileInfo(p) + "\t<->\t" + getFileInfo(mapFileSizes.get(size)));
                        } else if (size != 0 && size >= minSize && size <= maxSize) {
                            mapFileSizes.put(size, p);
                        }
                    } else if (recursively && Files.isDirectory(p)) {
                        findSame(p);
                    }
                } catch (IOException e) {
                    System.out.println("Unsuccessful file reading attempt");
                }
            });
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }

    public String getFileInfo(Path path) {
        StringBuilder sb = new StringBuilder();
        sb.append(path.getFileName().toString());
        sb.append("\t");
        sb.append(path);
        return sb.toString();
    }
}
