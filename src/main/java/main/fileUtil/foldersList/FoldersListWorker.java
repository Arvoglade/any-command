package main.fileUtil.foldersList;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Component
public class FoldersListWorker {
    public List<String> listFoldersForPaths(List<Path> paths, boolean recursively) {
        List<String> foldersList = new ArrayList<>();

        paths.stream()
                .map(path -> listFoldersForOnePath(path, 0))
                .forEach(foldersList::addAll);

        return foldersList;
    }

    private List<String> listFoldersForOnePath(Path path, int depth) {
        List<String> foldersList = new ArrayList<>();

        try {
            Files.list(path).filter(Files::isDirectory).forEach(d -> {
                foldersList.add("\t".repeat(depth) + d.toString());
                foldersList.addAll(listFoldersForOnePath(d, depth + 1));
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return foldersList;
    }
}
