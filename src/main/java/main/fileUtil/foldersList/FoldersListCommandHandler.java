package main.fileUtil.foldersList;

import main.core.Command;
import main.fileUtil.FilesSubcommandHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class FoldersListCommandHandler implements FilesSubcommandHandler {

    private Set<String> keys = new HashSet<>(List.of("-s"));

    @Autowired
    private FoldersListWorker foldersListWorker;

    @Override
    public String getName() {
        return "folders-list";
    }

    @Override
    public void handleCommand(Command command) {
        command = command.getCommandKeysFlags(keys, new HashSet<>()).getQuotedCommand();
        List<String> stringPaths = command.getKeysMultiple().get("-s");
        List<Path> paths = stringPaths.stream().map(Paths::get).toList();
        boolean recursively = true;
        List<String> foldersList = foldersListWorker.listFoldersForPaths(paths, recursively);
        foldersList.forEach(System.out::println);
    }

    @Override
    public String getHelp() {
        return "lists all folders in specified folders. use -s to specify folders";
    }
}
