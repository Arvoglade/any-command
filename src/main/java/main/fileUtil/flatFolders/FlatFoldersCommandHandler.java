package main.fileUtil.flatFolders;

import main.core.AbstractCommandHandler;
import main.core.Command;
import main.fileUtil.FilesSubcommandHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class FlatFoldersCommandHandler extends AbstractCommandHandler implements FilesSubcommandHandler {

    private final Set<String> keys = Set.of("-s", "-d");

    @Autowired
    private FlatFoldersWorker flatFoldersWorker;

    @Override
    public void handleCommand(Command command) {
        command = command.getCommandKeysFlags(keys, new HashSet<>()).getQuotedCommand();
        List<Path> sources = command.getKeysMultiple().get("-s").stream()
                .map(Paths::get).toList();
        Path destination = Paths.get(command.getKeys().get("-d"));
        flatFoldersWorker.flat(sources, destination);
    }

    @Override
    public void handle(Command command) {

    }

    @Override
    public Class<?> getSubcommandsInterface() {
        return null;
    }

    @Override
    public String getName() {
        return "flat-folders";
    }
}
