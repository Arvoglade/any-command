package main.fileUtil.copy;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Component
public class CopyWorker {


    public void copy(List<Path> sources, Path destination, List<String> patterns, boolean negatePattern) {
        sources.forEach(path -> copyForPath(path, destination, patterns, negatePattern));
    }

    private void copyForPath(Path source, Path destination, List<String> patterns, boolean negatePattern) {
        try {
            Files.list(source).forEach(path -> {
                if (Files.isDirectory(path)) {
                    copyForPath(path, destination, patterns, negatePattern);
                } else {
                    if (matchesForPatterns(path.getFileName().toString(), patterns, negatePattern)) {
                        try {
                            Files.move(path, destination.resolve(path.getFileName()));
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        System.out.println(path);
                    }
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean matchesForPatterns(String str, List<String> patterns, boolean negatePattern) {
        boolean anyMatches = patterns.stream().anyMatch(str::matches);
        return negatePattern ? !anyMatches : anyMatches;
    }

}





