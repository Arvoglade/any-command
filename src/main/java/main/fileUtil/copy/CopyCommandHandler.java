package main.fileUtil.copy;

import main.core.Command;
import main.core.exceptions.InvalidCommandException;
import main.fileUtil.FilesSubcommandHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class CopyCommandHandler implements FilesSubcommandHandler {

    private final Set<String> keys = Set.of("-s", "-d", "-p");
    private final Set<String> flags = Set.of("-i");
    @Autowired
    private CopyWorker copyWorker;


    @Override
    public String getName() {
        return "copy";
    }

    @Override
    public void handleCommand(Command command) {
        try {
            command = command.getCommandKeysFlags(keys, flags).getQuotedCommand();
            Path destination = Paths.get(command.getKeys().get("-d"));
            List<Path> sources = command.getKeysMultiple().get("-s").stream()
                    .map(Paths::get).toList();
            if (!command.getFlags().contains("-i")) {
            List<String> patterns = command.getKeysMultiple().get("-p");
                copyWorker.copy(sources, destination, patterns, false);
            } else {
                // files copy -s "f" -d"f" -p ".*\.mp4"
                List<String> patterns = List.of(
                        ".*\\.jpg",
                        ".*\\.jpeg",
                        ".*\\.jpg_large",
                        ".*\\.png",
                        ".*\\.bmp",
                        ".*\\.gif",
                        ".*\\.mp4",
                        ".*\\.webp",
                        ".*\\.webm"
                );
                copyWorker.copy(sources, destination, patterns, true);
            }
        } catch (Exception e) {
            throw new InvalidCommandException(e.getMessage());
        }
    }

    @Override
    public String getHelp() {
        return "copy -s source -d destination -p pattern";
    }
}
