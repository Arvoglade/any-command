package main.fileUtil;

import main.core.AbstractCommandHandler;
import main.core.Command;
import main.core.HeadCommandHandler;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilesCommandHandler extends AbstractCommandHandler implements HeadCommandHandler {

    private final Set<String> helpWords = new HashSet<>(List.of("help", "-h", "h", "--help"));


    @Override
    public String getName() {
        return "files";
    }

    @Override
    public void handle(Command command) {
        printManual();
    }

    @Override
    public String getHelp() {
        return "Files utils. Type \"files\" to get help";
    }

    @Override
    public Class<?> getSubcommandsInterface() {
        return FilesSubcommandHandler.class;
    }

    private void printManual() {
        System.out.println("Files utils");
        System.out.println("Syntax:" + "\n\t" + "files [subcommand]");
        System.out.println("List of subcommands");
        subcommandHandlersMap.entrySet().stream().forEach(e -> {
            System.out.print("\t" + e.getKey() + " - ");
            System.out.println(e.getValue().getHelp());
            System.out.println();
        });
    }
}
