package main.matrix;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class MatrixRunner implements Runnable {

    private Random random = new Random();
    static int screenHeight = 150;
    static int screenWidth = 300;
    static final String DENSITY = """
            @QB#NgWM8RDHdOKq9$6khEPXwmeZaoS2yjufF]}{tx1zv7lciL/\\|?*>r^;:_"~,'.-`""";


    @Override
    public void run() {

        Some.main("");

        //Process process = Runtime.getRuntime().exec("").getOutputStream().f;



        /*Scanner scanner = new Scanner(System.in);
        Runnable runnable = () -> System.out.println("RES: " + scanner.next());
        Thread thread = new Thread(runnable);
        thread.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }*/

        // TODO - эта строка сама выводит на экран результат. как считать последний вывод?
        //  Есть ли возможность переопределить стандартный вывод и там сделать буффер для последнего вывода
        //  Наверное проще считать данные с экрана используя другие ANSCII escape codes
        //System.out.println("RES: " + scanner.next());
        /*System.out.println(">");
        System.out.println("\u001b[6n");
        System.out.println("<");*/
        //System.out.println("RES: " + scanner.next());

        //getConsoleSize();

        //dropOneTear();
    }

    private void getConsoleSize() {
        Scanner scanner = new Scanner(System.in);

        // ^[[48;43R // образец получаемого результата



        // сдвиг вниз, чтобы корректно считать размер консоли. Потом можно попробовать его убрать и посмотреть что будет
        System.out.println("#\n".repeat(100));


        int maxColumn = 0; // использовать, чтобы в нужный момент прервать выполнение, когда значение перестанет расти
        while (true) {
            //System.out.print("Some long textSome long textSome long text");

            // TODO - закончил тут. Потребуется не выводить на экран то, что получаем из сканера,
            //  а испольовать в вычислниях. Это значит, что мне нужно будет получать результат с помощью Future
            //  Нужно вспомнить как использовать Future
            System.out.print(" ");
            //Runnable runnable = () -> System.out.println("Result: " + scanner.nextLine());
            Runnable runnable = () -> System.out.print("RES: " + parseConsoleSizes(scanner.nextLine()).toString());
            Thread thread = new Thread(runnable);
            thread.start();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            // TEST - только для теста. Убрать потом
            if (maxColumn++ > 50) return;

            System.out.print("\u001b[6n");
            //System.out.println("Request sent");
        }


    }

    private ConsoleSizes parseConsoleSizes(String sizesLine) {
        //String str = "^[[48;43R";

        sizesLine = sizesLine.substring(3);
        int indexOfSemicolon = sizesLine.indexOf(';');
        int indexOfR = sizesLine.indexOf('R');
        int height = Integer.parseInt(sizesLine.substring(0, indexOfSemicolon));
        int width = Integer.parseInt(sizesLine.substring(indexOfSemicolon + 1, indexOfR));
        return new ConsoleSizes(height, width);
    }

    static void printStringAt(String string, int row, int column) {
        String caretMoving = String.format("\033[%d;%dH", row, column);
        System.out.print(caretMoving);
        System.out.print(string);
    }

    static void clearScreen() {

        for (int row = 1; row < screenHeight; row++) {
            printStringAt(" ".repeat(screenWidth), row, 1);
        }

    }

    private void dropOneTear() {

        while (true) {
            clearScreen();
            int row = 1;
            int column = random.nextInt(screenWidth);

            int duringChar = 0;

            while (true) {
                String str = DENSITY.substring(duringChar, duringChar + 1);
                if (++duringChar >= DENSITY.length())
                    duringChar = 0;


                printStringAt(str, row, column);

                try {
                    Thread.sleep(25);
                } catch (InterruptedException e) {
                    return;
                }

                if (++row >= screenHeight) {
                    break;
                }

            }
        }
    }

    private class ConsoleSizes {
        int height;
        int width;
        ConsoleSizes(int height, int width) {
            this.height = height;
            this.width = width;
        }

        @Override
        public String toString() {
            return "(" + height + "," + width + ")";
        }
    }


}
