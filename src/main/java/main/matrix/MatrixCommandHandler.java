package main.matrix;

import main.core.Command;
import main.core.HeadCommandHandler;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class MatrixCommandHandler implements HeadCommandHandler {
    @Override
    public String getName() {
        return "matrix";
    }

    @Override
    public void handleCommand(Command command) {

        int row;
        int column;

        /*var params = command.getParams();
        row = Integer.parseInt(params.get(0));
        column = Integer.parseInt(params.get(1));*/

        Thread thread = new Thread(new MatrixRunner());
        thread.start();
        new Scanner(System.in).nextLine();
        thread.interrupt();
    }

    @Override
    public String getHelp() {
        return null;
    }
}
