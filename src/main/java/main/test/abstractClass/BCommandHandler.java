package main.test.abstractClass;

import main.core.AbstractCommandHandler;
import main.core.Command;
import main.test.TestSubcommandHandler;
import org.springframework.stereotype.Component;

@Component
public class BCommandHandler extends AbstractCommandHandler implements TestSubcommandHandler {
    @Override
    public String getName() {
        return "B";
    }

    @Override
    public void handle(Command command) {
        System.out.println("B was invoked");
    }

    @Override
    public Class<?> getSubcommandsInterface() {
        return null;
    }
}
