package main.test.abstractClass.a.g;

import main.core.Command;
import org.springframework.stereotype.Component;

@Component
public class JCommandHandler implements GSubcommandHandler {
    @Override
    public String getName() {
        return "J";
    }

    @Override
    public void handleCommand(Command command) {
        System.out.println("JJ");
    }

    @Override
    public String getHelp() {
        return null;
    }
}
