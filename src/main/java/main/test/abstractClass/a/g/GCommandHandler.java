package main.test.abstractClass.a.g;

import main.core.AbstractCommandHandler;
import main.core.Command;
import main.test.abstractClass.a.ASubcommandHandler;
import org.springframework.stereotype.Component;

@Component
public class GCommandHandler extends AbstractCommandHandler implements ASubcommandHandler {
    @Override
    public String getName() {
        return "G";
    }

    @Override
    public void handle(Command command) {
        System.out.println("GGGG");
    }

    @Override
    public Class<?> getSubcommandsInterface() {
        return GSubcommandHandler.class;
    }


}
