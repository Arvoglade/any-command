package main.test.abstractClass.a;

import main.core.AbstractCommandHandler;
import main.core.Command;
import main.test.TestSubcommandHandler;
import org.springframework.stereotype.Component;

@Component
public class ACommandHandler extends AbstractCommandHandler implements TestSubcommandHandler {

    @Override
    public String getName() {
        return "A";
    }


    @Override
    public void handle(Command command) {
        System.out.println("A has been called");
        System.out.println("A has been called");

    }

    @Override
    public Class<?> getSubcommandsInterface() {
        return ASubcommandHandler.class;
    }
}
