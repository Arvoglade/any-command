package main.test.testSubcommand;

import main.core.Command;
import main.test.TestSubcommandHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Command could get environment variable SOME_VAR.
 * In linux it works if variable has been set via linux command "export SOME_VAR=<some value>"
 */
@Component
public class TestEnvironmentVariable implements TestSubcommandHandler {

    @Value("${var}")
    private String var;

    @Override
    public String getName() {
        return "env";
    }

    @Override
    public void handleCommand(Command command) {
        System.out.println(var);
    }

    @Override
    public String getHelp() {
        return null;
    }
}
