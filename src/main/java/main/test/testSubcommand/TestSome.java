package main.test.testSubcommand;

import main.core.Command;
import main.test.TestSubcommandHandler;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class TestSome implements TestSubcommandHandler {

    private final ApplicationContext context;

    public TestSome(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public String getName() {
        return "some";
    }

    @Override
    public void handleCommand(Command command) {

    }

    @Override
    public String getHelp() {
        return null;
    }
}
