package main.test.testSubcommand.testInterruptable;

import main.core.Command;
import main.test.TestSubcommandHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

@Component
public class TestInterruptableCommand implements TestSubcommandHandler {

    public final SomeInterruptableWorker worker;

    public TestInterruptableCommand(SomeInterruptableWorker worker) {
        this.worker = worker;
    }

    @Override
    public String getName() {
        return "interruptable";
    }

    @Override
    public void handleCommand(Command command) {

        try {
            run();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public String getHelp() {
        return null;
    }

    public void run() throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        Thread thread = new Thread(new MyRunnable(Thread.currentThread(), scanner));
        thread.start();
        System.out.println("Workstarted");
        System.out.println("press Enter to stop");


        scanner.nextLine();
        thread.interrupt();

        System.out.println("END of command");
    }

    public class MyRunnable implements Runnable {

        private final Thread outsideThread;
        private final Scanner scanner;

        public MyRunnable(Thread outsideThread, Scanner scanner) {
            this.outsideThread = outsideThread;
            this.scanner = scanner;
        }

        @Override
        public void run() {
            try {
                TestInterruptableCommand.this.worker.doWork();
                System.out.println("work is done");
            } catch (InterruptedException e) {
                System.out.println("Work stopped outside too");
            }
            //outsideThread.interrupt();
        }
    }
}
