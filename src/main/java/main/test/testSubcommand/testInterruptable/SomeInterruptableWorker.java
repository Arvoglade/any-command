package main.test.testSubcommand.testInterruptable;

import org.springframework.stereotype.Component;

@Component
public class SomeInterruptableWorker {


    public void doWork() throws InterruptedException {
        double sum = 0;
        for (int i = 0; i < 2; i++) {
            sum = 0;
            for (int j = 0; j < 10000; j++) {

                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("work stopped");
                    // return;
                    throw new InterruptedException();
                }

                for (int k = 0; k < 10000; k++) {
                    sum += Math.sin(i) + Math.sin(j) + Math.sin(k);
                }

            }
            System.out.println("SUM: " + sum);
        }
    }

}
