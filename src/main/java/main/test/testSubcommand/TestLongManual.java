package main.test.testSubcommand;

import main.core.Command;
import main.core.exceptions.CommandException;
import main.test.TestSubcommandHandler;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class TestLongManual implements TestSubcommandHandler {
    @Override
    public String getName() {
        return "long-man";
    }

    @Override
    public void handleCommand(Command command) throws CommandException {

        String longText = getLongText(500);
        int linesToShowByOneTime = 10;

        printLongTextBySmallPieces(longText, linesToShowByOneTime);
    }

    @Override
    public String getHelp() {
        return null;
    }

    private String getLongText(int linesCount) {
        StringBuilder sb = new StringBuilder("Some very long text\n");
        for (int i = 0; i < linesCount; i++) {
            sb.append(i + 1);
            sb.append(" another line\n");
        }
        sb.append("End of the text");
        return sb.toString();
    }

    public static void printLongTextBySmallPieces(String text, int linesToShowByOneTime) {
        String[] textLines = text.split("\n");
        Scanner scanner = new Scanner(System.in);

        int shownLines = 0;
        while (shownLines < textLines.length) {
            int i = 0;
            while (i < linesToShowByOneTime && shownLines + i < textLines.length) {
                System.out.println(textLines[shownLines + i]);
                i++;
            }
            shownLines += linesToShowByOneTime;
            System.out.println("enter - to continue. q - to quit");
            String letter = scanner.nextLine();
            if (letter.equals("q"))
                break;
        }
    }

}
