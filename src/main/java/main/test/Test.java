package main.test;

import main.core.Command;
import main.core.HeadCommandHandler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class Test implements HeadCommandHandler {


    private final Map<String, TestSubcommandHandler> testCommandHandlerMap;

    public Test(List<TestSubcommandHandler> testCommandHandlerList) {
        this.testCommandHandlerMap = testCommandHandlerList.stream().collect(Collectors.toMap(ch -> ch.getName(), ch -> ch));
    }


    @Override
    public String getName() {
        return "test";
    }

    @Override
    public void handleCommand(Command command) {
        if (command.getParams().size() == 0) {
            System.out.println(getHelp());
        } else if (command.get(0).equals("list")) {
            System.out.println("List of test-commands: ");
            for (String testCommandName : testCommandHandlerMap.keySet()) {
                System.out.println("\t" + testCommandName);
            }
        } else {
            findAndHandleSubcommand(command, 1, testCommandHandlerMap);
        }
    }

    @Override
    public String getHelp() {
        return """
                For testing some commands. Type \"test <command name>\"  to make test. Type \"test list\" to list all test-commands""";
    }

}
